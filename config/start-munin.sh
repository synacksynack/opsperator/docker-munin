#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/munin-nsswrapper.sh

if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=munin.demo.lan
fi
MUNIN_COLLECT_INTERVAL=${MUNIN_COLLECT_INTERVAL:-1}
mkdir -p /etc/munin/munin-conf.d

monitorHost()
{
    local address hostname port
    hostname=$1
    address=$2
    if test "$3"; then
	port=$3
    else
	port=4949
    fi
    cat <<EOF >/etc/munin/munin-conf.d/$hostname.conf
[$hostname]
    address $address
    port $port
    use_node_name no
EOF
}

if test "$MONITOR_SELF"; then
    monitorHost $APACHE_DOMAIN 127.0.0.1
fi
for entry in $MONITOR_NODES
do
    a=`echo $entry | cut -d: -f1`
    n=`echo $entry | cut -d: -f2`
    p=`echo $entry | cut -d: -f3`
    monitorHost $a $n $p
done

if test "$MAILDOMAIN"; then
    MAILDOMAIN=${MAILDOMAIN:-demo.lan}
    MAILPASSWORD=${MAILPASSWORD:-}
    MAILPORT=${MAILPORT:-25}
    MAILSTARTTLS=${MAILSTARTTLS:-NO}
    MAILTLS=${MAILTLS:-NO}
    MAILUSER=${MAILUSER:-}
    test -z "$MAILCONTACT" && MAILCONTACT=contact@$MAILDOMAIN
    test -z "$MAILSERVER" && MAILSERVER=mail.$MAILDOMAIN
    test -z "$MAILFROM" && MAILFROM=munin@$MAILDOMAIN
    cat <<EOF >/etc/munin/munin-conf.d/munin_mail.conf
contact.hosting.command mail -s "[MUNIN] Alert \${var:graph_title} for \${var:host}" $MAILCONTACT
contact.hosting.max_messages 1
contact.no.command awk '{ print "[MUNIN] Alert \${var:graph_title} for \${var:host}" }' /dev/null
EOF
    if test "$FIXME"; then
	cat <<EOF >/etc/ssmtp/ssmtp.conf
root=$MAILUSER
mailhub=$MAILHOST:$MAILPORT
AuthUser=$MAILUSER
AuthPass=$MAILPASS
UseTLS=$MAILTLS
UseSTARTTLS=$MAILSTARTTLS
rewriteDomain=$MAILDOMAIN
FromLineOverride=NO
EOF
	cat <<EOF >/etc/ssmtp/revaliases
munin:$MAILFROM:$MAILSERVER:$MAILPORT
nobody:$MAILFROM:$MAILSERVER:$MAILPORT
root:$MAILFROM:$MAILSERVER:$MAILPORT
EOF
    fi
else
    rm -f /etc/ssmtp/ssmtp.conf
fi

mkdir -p /var/lib/munin/cgi-tmp

RESET_CACHE=false
RESET_LIMITS=false
RESET_TMP=false
interval=`expr $MUNIN_COLLECT_INTERVAL '*' 60`
while :
do
    started=`date +%s`
    if test -x /usr/bin/munin-cron; then
	/usr/bin/munin-cron
    fi
    if date +%H | grep 10 >/dev/null; then
	if ! $RESET_LIMITS; then
	    if date +%M | grep -E '[1-3][0-9]' >/dev/null; then
		if test -x /usr/share/munin/munin-limits; then
		    /usr/share/munin/munin-limits --force \
			--contact nagios --contact old-nagios
			RESET_LIMITS=true
		fi
	    fi
	fi
    else
	RESET_LIMITS=false
    fi
    if date +%H | grep 3 >/dev/null; then
	if ! $RESET_CACHE; then
	    if date +%M | grep -E '[0-2][0-9]' >/dev/null; then
		if test -d /var/cache/munin/www; then
		    find /var/cache/munin/www/ -type f -name "*.html" -mtime +30 -delete
		    find /var/cache/munin/www/ -mindepth 1 -type d -empty -delete
		    RESET_CACHE=true
		fi
	    fi
	fi
	if date +%M | grep -E '[3-5][0-9]' >/dev/null; then
	    if ! $RESET_TMP; then
		if test -d /var/lib/munin/cgi-tmp; then
		    find /var/lib/munin/cgi-tmp/ -type f -mtime +1 -delete
		    find /var/lib/munin/cgi-tmp/ -mindepth 1 -type d -empty -delete
		    RESET_TMP=true
		fi
	    fi
	fi
    else
	RESET_CACHE=false
	RESET_TMP=false
    fi

    stopped=`date +%s`
    elapsed=`expr $stopped - $started`
    waitfor=`expr $interval - $elapsed`
    if test "$waitfor" -le 10; then
	waitfor=$interval
    fi
    echo "Done running - took ${elapsed}s, sleeping for ${waitfor}s"
    sleep $waitfor
done

exit 1
