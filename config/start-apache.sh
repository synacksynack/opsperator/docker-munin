#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=munin,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=munin.demo.lan
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$OPENLDAP_SEARCH" -a "$AUTH_METHOD" = ldap; then
    OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
elif test "$AUTH_METHOD" = oidc -a -z "$OIDC_PORTAL"; then
    OIDC_PORTAL=$PUBLIC_PROTO://auth.$OPENLDAP_DOMAIN
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=true
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

mkdir -p /etc/munin/mod_fcgid
if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo Generates Munin VirtualHost Configuration

    if test "$AUTH_METHOD" = oidc; then
	if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
	    OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	fi
	CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
	    CALLBACK_ROOT_SUB_COUNT=2
	fi
	CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
	sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|g" \
	    -e "s|APACHE_HTTP_PORT|$APACHE_HTTP_PORT|g" \
	    -e "s|CALLBACK_ROOT_URL|$CALLBACK_ROOT_URL|g" \
	    -e "s|OIDC_CALLBACK_URL|$OIDC_CALLBACK_URL|g" \
	    -e "s|OIDC_CLIENT_ID|$OIDC_CLIENT_ID|g" \
	    -e "s|OIDC_CLIENT_SECRET|$OIDC_CLIENT_SECRET|g" \
	    -e "s|OIDC_CRYPTO_SECRET|$OIDC_CRYPTO_SECRET|g" \
	    -e "s|OIDC_META_URL|$OIDC_META_URL|g" \
	    -e "s|OIDC_PORTAL|$OIDC_PORTAL|g" \
	    -e "s|OIDC_TOKEN_ENDPOINT_AUTH|$OIDC_TOKEN_ENDPOINT_AUTH|g" \
	    -e "s|PUBLIC_PROTO|$PUBLIC_PROTO|g" \
	    -e "s|SSL_INCLUDE|$SSL_INCLUDE|g" \
	    /vhost-oidc.conf >/etc/apache2/sites-enabled/003-vhosts.conf
    elif test "$AUTH_METHOD" = ldap; then
	sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|g" \
	    -e "s|APACHE_HTTP_PORT|$APACHE_HTTP_PORT|g" \
	    -e "s|OPENLDAP_BASE|$OPENLDAP_BIND_DN_PREFIX|g" \
	    -e "s|OPENLDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|g" \
	    -e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|g" \
	    -e "s|OPENLDAP_URL|$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT|g" \
	    -e "s|USERS_OBJECTCLASS|$OPENLDAP_USERS_OBJECTCLASS|g" \
	    -e "s|SSL_INCLUDE|$SSL_INCLUDE|g" \
	    /vhost-ldap.conf >/etc/apache2/sites-enabled/003-vhosts.conf
    else
	sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|g" \
	    -e "s|APACHE_HTTP_PORT|$APACHE_HTTP_PORT|g" \
	    -e "s|SSL_INCLUDE|$SSL_INCLUDE|g" \
	    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf
    fi
fi

if test "$AUTH_METHOD" = oidc; then
    if test "$PUBLIC_PROTO" = https -a "$OIDC_SKIP_TLS_VERIFY"; then
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    else
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    fi
    sed -i \
	-e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	-e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	-e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	-e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	-e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	-e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	/etc/apache2/mods-enabled/auth_openidc.conf
fi

if ! test -f /var/cache/munin/www/index.html; then
    cat <<EOF >/var/cache/munin/www/index.html
<html>
  <head>
    <title>Munin</title>
  </head>
  <body>
    Munin has not run yet.<br/>
    Please try again in a few moments.
  </body>
</html>
EOF
fi

while ! test -d /var/lib/munin/cgi-tmp
do
    echo Waiting for Munin to start up...
    sleep 5
done

set -- /usr/sbin/apache2ctl -D FOREGROUND
. /run-apache.sh
