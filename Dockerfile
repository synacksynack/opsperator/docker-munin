FROM opsperator/apache

# Munin image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="Munin Server, perl based metrology." \
      io.k8s.display-name="Munin" \
      io.openshift.expose-services="8080:http,4949:munin" \
      io.openshift.tags="munin,munin-node" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-munin" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.0.49"

USER root

COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Munin" \
    && apt-get install -y munin wget libapache2-mod-fcgid libcgi-fast-perl \
	msmtp mailutils curl tzdata autoconf munin-node munin-plugins-extra \
	munin-plugins-core \
    && mv /munin.conf /etc/munin/ \
    && mv /munin-nsswrapper.sh /usr/local/bin/ \
    && rm -fr /etc/munin/plugin-conf.d/* /etc/munin/plugins/* \
	/var/lib/munin-node/plugin-state /etc/munin/munin-node.conf \
    && mv /plugins.conf /etc/munin/plugin-conf.d/munin-node.conf \
    && echo "# Configure Apache" \
    && echo "FcgidIPCDir /etc/munin/mod_fcgid" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && echo "FcgidProcessTableFile /etc/munin/mod_fcgid/fcgid_shm" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && a2enmod fcgid ldap authnz_ldap auth_openidc \
    && a2dismod perl \
    && echo "# Fixing permissions" \
    && mkdir -p /var/run/munin /var/lib/munin /var/lib/munin/cgi-tmp \
	/etc/munin/munin-conf.d /var/lib/munin-node/plugin-state \
	/var/cache/munin \
    && chown -R 1001:0 /var/run/munin /var/lib/munin /var/cache/munin \
	/etc/munin /var/lib/munin-node/plugin-state \
    && chmod -R g=u /var/run/munin /var/lib/munin /var/cache/munin /etc/munin \
	/var/lib/munin-node/plugin-state \
    && echo "# Cleaning up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/start-munin.sh"]
USER 1001
