# k8s Munin

Munin image for Kubernetes

Diverts from https://gitlab.com/synacksynack/opsperator/docker-apache

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                  | Default                                                     | Inherited From    |
| :----------------------------- | ------------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`               | Munin ServerName                | `munin.demo.local`                                          | opsperator/apache |
|  `APACHE_HTTP_PORT`            | Munin HTTP(s) Port              | `8080`                                                      | opsperator/apache |
|  `MONITOR_NODES`               | Munin Nodes to Monitor          | undef, eg: `nodename1:1.2.3.4 nodename2:1.2.3.4:4950`       |                   |
|  `MAILCONTACT`                 | Munin Mail Recipient            | `contact@$MAILDOMAIN`                                       |                   |
|  `MAILDOMAIN`                  | Munin Mail Domain               | undef                                                       |                   |
|  `MAILFROM`                    | Munin Mail From                 | `munin@$MAILDOMAIN`                                         |                   |
|  `MAILPASSWORD`                | Munin Mail SMTP Login Password  | undef                                                       |                   |
|  `MAILPORT`                    | Munin SMTP Port                 | `25`                                                        |                   |
|  `MAILSERVER`                  | Munin SMTP Server               | `mail.$MAILDOMAIN`                                          |                   |
|  `MAILSTARTTLS`                | Munin Mail Uses StartTLS        | undef                                                       |                   |
|  `MAILTLS`                     | Munin Mail Uses TLS             | undef                                                       |                   |
|  `MAILUSER`                    | Munin Mail SMTP Login User      | undef                                                       |                   |
|  `MUNIN_COLLECT_INTERVAL`      | Munin Stats Collection Interval | `1`, in minutes                                             |                   |
|  `PUBLIC_PROTO`                | Public Munin Proto              | undef, assumes `http`                                       | opsperator/apache |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point        | Description                      | Inherited From    |
| :------------------------- | -------------------------------- | ----------------- |
|  `/certs`                  | Apache Certificate (optional)    | opsperator/apache |
|  `/var/lib/munin`          | Munin Metrics Storage            |                   |
|  `/var/cache/munin/www`    | Munin Site Root                  |                   |
|  `/var/log/munin`          | Munin-generated Logs             |                   |
