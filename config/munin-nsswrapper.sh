#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/munin-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to munin
	sed "s|^munin:.*|munin:x:`id -g`:|" /etc/group >/tmp/munin-group
	sed \
	    "s|^munin:.*|munin:x:`id -u`:`id -g`:Munin:/var/lib/munin:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/munin-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/munin-passwd
    export NSS_WRAPPER_GROUP=/tmp/munin-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
