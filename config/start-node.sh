#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/munin-nsswrapper.sh

if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=munin.demo.lan
fi
if test -z "$MUNIN_NODENAME"; then
    MUNIN_NODENAME=$APACHE_DOMAIN
fi
if test -z "$MUNIN_ALLOW"; then
    MUNIN_ALLOW=".*"
fi

sed -e "s|NODENAME|$MUNIN_NODENAME|g" \
    -e "s|ALLOWHOST|$MUNIN_ALLOW|g" \
    /node.conf >/etc/munin/munin-node.conf

for u in root nobody munin
do
    mkdir -p /var/lib/munin-node/plugin-state/$u
done

if test -z "$DEFAULT_PLUGINS"; then
    DEFAULT_PLUGINS="acpi cpu cpuspeed df df_inode diskstats entropy
	forks fw_conntrack fw_forwarded_local fw_packets interrupts
	irqstats load memory open_files open_inodes proc_pri processes
	threads uptime vmstat"
fi
if test "$APACHE_DOMAIN" = "$MUNIN_NODENAME"; then
    DEFAULT_PLUGINS="$DEFAULT_PLUGINS munin_stats"
fi
ENABLE_PLUGINS="$ENABLE_PLUGINS $DEFAULT_PLUGINS"
for plugin in $ENABLE_PLUGINS
do
    if echo $plugin | grep : >/dev/null; then
	plg=`echo $plugin | cut -d: -f1`
	probe=`echo $plugin | cut -d: -f2`
	file=${plg}_$probe
    else
	file=$plugin
	plg=$plugin
    fi
    if test -s /usr/share/munin/custom-plugins/$plg; then
	cat /usr/share/munin/custom-plugins/$plg >/etc/munin/plugins/$file
	chmod +x /etc/munin/plugins/$file
	echo "NOTICE: enabled $plg as $file"
    elif test -s /usr/share/munin/plugins/$plg; then
	cat /usr/share/munin/plugins/$plg >/etc/munin/plugins/$file
	chmod +x /etc/munin/plugins/$file
	echo "NOTICE: enabled $plg as $file"
    else
	echo "WARNING: could not find plugin $plg, skipping $file"
    fi
done
if test -d /host; then
    echo NOTICE: detected /host, patching probes accordingly
    find /etc/munin/plugins -type f | while read probe
	do
	    sed -i \
		-e "s| /proc| /host/proc|g" \
		-e "s| /sys| /host/sys|g" \
		"$probe"
	done
fi
if test -s /custom/config.conf; then
    echo NOTICE: installing custom configuration
    cat /custom/config.conf >/etc/munin/plugin-conf.d/runtime.conf
fi

exec /usr/sbin/munin-node
