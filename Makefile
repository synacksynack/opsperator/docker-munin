SKIP_SQUASH?=1
IMAGE = opsperator/munin
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testmunin || true
	@@docker run --name testmunin \
	    --entrypoint dumb-init \
	    -e APACHE_DOMAIN=munin.demo.local \
	    -d $(IMAGE) \
	    /start-apache.sh

.PHONY: demo
demo: run

.PHONY: run
run:
	@@mkdir -p data/log data/cache data/lib
	@@chmod 777 data/log data/cache data/lib
	@@docker-compose up

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service deployment rbac daemonset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done
